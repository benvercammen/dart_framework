enum DateTimeRepresentation {
  iso8601String,
  millisecondsSinceEpoch,
  microsecondsSinceEpoch,
}
