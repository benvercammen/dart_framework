## 2.1.0

### Changes
- Moved source code repository from GitHub to GitLab
- Adopted Melos and GitLab as CI solution
- Changed license to MPL

### Rationale for source code hosting change

The change from source code repository is in protest by this project's initial and main author with what he views as [GitHub's extremely week response](https://github.blog/2022-03-02-our-response-to-the-war-in-ukraine/) to the carnage going on in Ukraine by Russia. He would have expected at a minimum for any new business in Russia and Belarus to be suspended, which was incidentally [GitLab's course of action](https://about.gitlab.com/blog/2022/03/11/gitlab-actions-to-date-regarding-russian-invasion-of-ukraine/#suspending-new-business-in-russia-and-belarus).


## 2.0.5

- Restored adding revision on update


## 2.0.4

- More modular implementation of ArangoDBRepository

## 2.0.3

- Fixed example

## 2.0.2

- Added support for optimistic concurrency

## 2.0.1

- Updated arango_driver dependency to 2.0.1

## 2.0.0

- KeyHandler is no longer a dependency, but a build configuration.

## 1.0.3

- Added reference to integrating framework.

## 1.0.2

- Moved code to monorepo

## 1.0.1

- Changed license to MIT

## 1.0.0

- Release

## 1.0.0-rc.2

- Merged repository and search

## 1.0.0-rc.1

- Search is working

## 0.9.0

- Initial version.

