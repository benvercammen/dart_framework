## 3.1.2
- Added missing type parameters to HostParameters settings field

## 3.1.1
- Fixed missing exports

## 3.1.0
- Removed deprecated smtp configuration support.
- Supporting classless hosts

## 3.0.8

- Included idToken in ServiceCall extension

## 3.0.7

- Upgraded dependencies

## 3.0.6

- Upgraded dependencies

## 3.0.5

- Upgraded dependencies

## 3.0.4

- Support for empty SslSettings

## 3.0.3

- Added copyWith to SslSettings

## 3.0.2

- Added copyWith to HostSettings

## 3.0.1

- Deprecating smtpSettings

## 3.0.0

- Split host settings from customizable AppSettings

## 2.0.0

- Breaking change: Removed properties from HostSettings that are not directly related to hosting

## 1.0.1

- Expanded copyright to include 'authors'

## 1.0.0

- Initial version