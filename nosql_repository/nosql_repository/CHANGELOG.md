## 2.1.1

- Fixed copyright notice.

## 2.1.0

### Changes
- Moved source code repository from GitHub to GitLab
- Adopted Melos and GitLab as CI solution
- Changed license to MPL

### Rationale for source code hosting change

The change from source code repository is in protest by this project's initial and main author with what he views as [GitHub's extremely week response](https://github.blog/2022-03-02-our-response-to-the-war-in-ukraine/) to the carnage going on in Ukraine by Russia. He would have expected at a minimum for any new business in Russia and Belarus to be suspended, which was incidentally [GitLab's course of action](https://about.gitlab.com/blog/2022/03/11/gitlab-actions-to-date-regarding-russian-invasion-of-ukraine/#suspending-new-business-in-russia-and-belarus).


## 2.0.0

- Updated dependencies

## 1.1.1

- Added reference to integrating framework.

## 1.1.0

- Version bump

## 1.0.2

- Moved code to monorepo

## 1.0.1

- Changed license to MIT

## 1.0.0

- Release

## 1.0.0-rc.6

- Added not_found and unauthorized error classes

## 1.0.0-rc.5

- Added isAuthorized to createPolicy

## 1.0.0-rc.4

- Added missing export to createPolicy

## 1.0.0-rc.3

- Created permission policy concept

## 1.0.0-rc.2

- Merged repository and search classes

## 1.0.0-rc.1

- Bumped to 1.0.0-rc.1

## 0.9.3

- permission parameters made non-nullable

## 0.9.2

- Repository.runQuery parameters made non-nullable

## 0.9.1

- SearchCriteria lists made non-nullable

## 0.9.0

- Initial version.
