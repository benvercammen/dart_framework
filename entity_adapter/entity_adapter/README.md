Annotations used by the [squarealfa_entity_adapter_generator](https://pub.dev/packages/squarealfa_entity_adapter_generator) package.

## Getting started

In order to get started, look at the example project at https://gitlab.com/squarealfa/dart_framework/tree/main/entity_adapter/example.


## Context

This package is part of a set of losely integrated packages that constitute the [SquareAlfa Dart Framework](https://gitlab.com/squarealfa/dart_framework#squarealfa-dart-framework).
