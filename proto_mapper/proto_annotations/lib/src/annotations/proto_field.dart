class ProtoField {
  final String? name;
  final int? number;
  final int? hasValueNumber;

  const ProtoField({
    this.name,
    this.number,
    this.hasValueNumber,
  });
}

const protoField = ProtoField();
