///
//  Generated code. Do not modify.
//  source: calc_parameters.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use gCalcParametersDescriptor instead')
const GCalcParameters$json = {
  '1': 'GCalcParameters',
  '2': [
    {'1': 'parameter1', '3': 2, '4': 1, '5': 5, '10': 'parameter1'},
    {'1': 'parameter2', '3': 1, '4': 1, '5': 5, '10': 'parameter2'},
    {'1': 'parameter3', '3': 3, '4': 1, '5': 5, '10': 'parameter3'},
    {
      '1': 'parameter3_has_value',
      '3': 4,
      '4': 1,
      '5': 8,
      '10': 'parameter3HasValue'
    },
    {'1': 'parameter4', '3': 5, '4': 1, '5': 5, '10': 'parameter4'},
    {
      '1': 'parameter4_has_value',
      '3': 6,
      '4': 1,
      '5': 8,
      '10': 'parameter4HasValue'
    },
    {'1': 'parameter5', '3': 9, '4': 1, '5': 5, '10': 'parameter5'},
  ],
};

/// Descriptor for `GCalcParameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gCalcParametersDescriptor = $convert.base64Decode(
    'Cg9HQ2FsY1BhcmFtZXRlcnMSHgoKcGFyYW1ldGVyMRgCIAEoBVIKcGFyYW1ldGVyMRIeCgpwYXJhbWV0ZXIyGAEgASgFUgpwYXJhbWV0ZXIyEh4KCnBhcmFtZXRlcjMYAyABKAVSCnBhcmFtZXRlcjMSMAoUcGFyYW1ldGVyM19oYXNfdmFsdWUYBCABKAhSEnBhcmFtZXRlcjNIYXNWYWx1ZRIeCgpwYXJhbWV0ZXI0GAUgASgFUgpwYXJhbWV0ZXI0EjAKFHBhcmFtZXRlcjRfaGFzX3ZhbHVlGAYgASgIUhJwYXJhbWV0ZXI0SGFzVmFsdWUSHgoKcGFyYW1ldGVyNRgJIAEoBVIKcGFyYW1ldGVyNQ==');
@$core.Deprecated('Use gListOfCalcParametersDescriptor instead')
const GListOfCalcParameters$json = {
  '1': 'GListOfCalcParameters',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GCalcParameters',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfCalcParameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfCalcParametersDescriptor = $convert.base64Decode(
    'ChVHTGlzdE9mQ2FsY1BhcmFtZXRlcnMSJgoFaXRlbXMYASADKAsyEC5HQ2FsY1BhcmFtZXRlcnNSBWl0ZW1z');
