///
//  Generated code. Do not modify.
//  source: polymorphism/scooter.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use gScooterDescriptor instead')
const GScooter$json = {
  '1': 'GScooter',
  '2': [
    {'1': 'weight', '3': 1, '4': 1, '5': 5, '10': 'weight'},
  ],
};

/// Descriptor for `GScooter`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gScooterDescriptor =
    $convert.base64Decode('CghHU2Nvb3RlchIWCgZ3ZWlnaHQYASABKAVSBndlaWdodA==');
@$core.Deprecated('Use gListOfScooterDescriptor instead')
const GListOfScooter$json = {
  '1': 'GListOfScooter',
  '2': [
    {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.GScooter', '10': 'items'},
  ],
};

/// Descriptor for `GListOfScooter`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfScooterDescriptor = $convert.base64Decode(
    'Cg5HTGlzdE9mU2Nvb3RlchIfCgVpdGVtcxgBIAMoCzIJLkdTY29vdGVyUgVpdGVtcw==');
