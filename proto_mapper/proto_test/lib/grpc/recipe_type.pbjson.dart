///
//  Generated code. Do not modify.
//  source: recipe_type.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use gRecipeTypesDescriptor instead')
const GRecipeTypes$json = {
  '1': 'GRecipeTypes',
  '2': [
    {'1': 'G_RECIPE_TYPES_COOK', '2': 0},
    {'1': 'G_RECIPE_TYPES_GRILL', '2': 1},
    {'1': 'G_RECIPE_TYPES_FRY', '2': 2},
    {'1': 'G_RECIPE_TYPES_STEW', '2': 3},
  ],
};

/// Descriptor for `GRecipeTypes`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List gRecipeTypesDescriptor = $convert.base64Decode(
    'CgxHUmVjaXBlVHlwZXMSFwoTR19SRUNJUEVfVFlQRVNfQ09PSxAAEhgKFEdfUkVDSVBFX1RZUEVTX0dSSUxMEAESFgoSR19SRUNJUEVfVFlQRVNfRlJZEAISFwoTR19SRUNJUEVfVFlQRVNfU1RFVxAD');
@$core.Deprecated('Use nullableGRecipeTypesDescriptor instead')
const NullableGRecipeTypes$json = {
  '1': 'NullableGRecipeTypes',
  '2': [
    {'1': 'has_value', '3': 1, '4': 1, '5': 8, '10': 'hasValue'},
    {
      '1': 'value',
      '3': 2,
      '4': 1,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'value'
    },
  ],
};

/// Descriptor for `NullableGRecipeTypes`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nullableGRecipeTypesDescriptor = $convert.base64Decode(
    'ChROdWxsYWJsZUdSZWNpcGVUeXBlcxIbCgloYXNfdmFsdWUYASABKAhSCGhhc1ZhbHVlEiMKBXZhbHVlGAIgASgOMg0uR1JlY2lwZVR5cGVzUgV2YWx1ZQ==');
