///
//  Generated code. Do not modify.
//  source: recipe_services_base.services.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use g_GRecipeService_Delete_ReturnDescriptor instead')
const G_GRecipeService_Delete_Return$json = {
  '1': 'G_GRecipeService_Delete_Return',
};

/// Descriptor for `G_GRecipeService_Delete_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_Delete_ReturnDescriptor =
    $convert.base64Decode('Ch5HX0dSZWNpcGVTZXJ2aWNlX0RlbGV0ZV9SZXR1cm4=');
@$core.Deprecated('Use g_GRecipeService_Search_ParametersDescriptor instead')
const G_GRecipeService_Search_Parameters$json = {
  '1': 'G_GRecipeService_Search_Parameters',
};

/// Descriptor for `G_GRecipeService_Search_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_Search_ParametersDescriptor =
    $convert.base64Decode('CiJHX0dSZWNpcGVTZXJ2aWNlX1NlYXJjaF9QYXJhbWV0ZXJz');
@$core.Deprecated(
    'Use g_GRecipeService_SearchNullable_ParametersDescriptor instead')
const G_GRecipeService_SearchNullable_Parameters$json = {
  '1': 'G_GRecipeService_SearchNullable_Parameters',
};

/// Descriptor for `G_GRecipeService_SearchNullable_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_SearchNullable_ParametersDescriptor =
    $convert.base64Decode(
        'CipHX0dSZWNpcGVTZXJ2aWNlX1NlYXJjaE51bGxhYmxlX1BhcmFtZXRlcnM=');
@$core
    .Deprecated('Use g_GRecipeService_SearchNullable_ReturnDescriptor instead')
const G_GRecipeService_SearchNullable_Return$json = {
  '1': 'G_GRecipeService_SearchNullable_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 3, '5': 11, '6': '.GRecipe', '10': 'value'},
    {'1': 'value_has_value', '3': 2, '4': 1, '5': 8, '10': 'valueHasValue'},
  ],
};

/// Descriptor for `G_GRecipeService_SearchNullable_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_SearchNullable_ReturnDescriptor =
    $convert.base64Decode(
        'CiZHX0dSZWNpcGVTZXJ2aWNlX1NlYXJjaE51bGxhYmxlX1JldHVybhIeCgV2YWx1ZRgBIAMoCzIILkdSZWNpcGVSBXZhbHVlEiYKD3ZhbHVlX2hhc192YWx1ZRgCIAEoCFINdmFsdWVIYXNWYWx1ZQ==');
@$core.Deprecated('Use g_GRecipeService_InsertMany_ReturnDescriptor instead')
const G_GRecipeService_InsertMany_Return$json = {
  '1': 'G_GRecipeService_InsertMany_Return',
};

/// Descriptor for `G_GRecipeService_InsertMany_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_InsertMany_ReturnDescriptor =
    $convert.base64Decode('CiJHX0dSZWNpcGVTZXJ2aWNlX0luc2VydE1hbnlfUmV0dXJu');
@$core
    .Deprecated('Use g_GRecipeService_GetNullable_ParametersDescriptor instead')
const G_GRecipeService_GetNullable_Parameters$json = {
  '1': 'G_GRecipeService_GetNullable_Parameters',
};

/// Descriptor for `G_GRecipeService_GetNullable_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_GetNullable_ParametersDescriptor =
    $convert.base64Decode(
        'CidHX0dSZWNpcGVTZXJ2aWNlX0dldE51bGxhYmxlX1BhcmFtZXRlcnM=');
@$core.Deprecated('Use g_GRecipeService_GetNullable_ReturnDescriptor instead')
const G_GRecipeService_GetNullable_Return$json = {
  '1': 'G_GRecipeService_GetNullable_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 1, '5': 11, '6': '.GRecipe', '10': 'value'},
    {'1': 'value_has_value', '3': 2, '4': 1, '5': 8, '10': 'valueHasValue'},
  ],
};

/// Descriptor for `G_GRecipeService_GetNullable_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_GetNullable_ReturnDescriptor =
    $convert.base64Decode(
        'CiNHX0dSZWNpcGVTZXJ2aWNlX0dldE51bGxhYmxlX1JldHVybhIeCgV2YWx1ZRgBIAEoCzIILkdSZWNpcGVSBXZhbHVlEiYKD3ZhbHVlX2hhc192YWx1ZRgCIAEoCFINdmFsdWVIYXNWYWx1ZQ==');
@$core.Deprecated('Use g_GRecipeService_Count_ParametersDescriptor instead')
const G_GRecipeService_Count_Parameters$json = {
  '1': 'G_GRecipeService_Count_Parameters',
};

/// Descriptor for `G_GRecipeService_Count_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_Count_ParametersDescriptor =
    $convert.base64Decode('CiFHX0dSZWNpcGVTZXJ2aWNlX0NvdW50X1BhcmFtZXRlcnM=');
@$core.Deprecated('Use g_GRecipeService_Count_ReturnDescriptor instead')
const G_GRecipeService_Count_Return$json = {
  '1': 'G_GRecipeService_Count_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 1, '5': 5, '10': 'value'},
  ],
};

/// Descriptor for `G_GRecipeService_Count_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_Count_ReturnDescriptor =
    $convert.base64Decode(
        'Ch1HX0dSZWNpcGVTZXJ2aWNlX0NvdW50X1JldHVybhIUCgV2YWx1ZRgBIAEoBVIFdmFsdWU=');
@$core.Deprecated(
    'Use g_GRecipeService_CountNullable_ParametersDescriptor instead')
const G_GRecipeService_CountNullable_Parameters$json = {
  '1': 'G_GRecipeService_CountNullable_Parameters',
  '2': [
    {'1': 'return_null', '3': 1, '4': 1, '5': 8, '10': 'returnNull'},
  ],
};

/// Descriptor for `G_GRecipeService_CountNullable_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_CountNullable_ParametersDescriptor = $convert.base64Decode(
        'CilHX0dSZWNpcGVTZXJ2aWNlX0NvdW50TnVsbGFibGVfUGFyYW1ldGVycxIfCgtyZXR1cm5fbnVsbBgBIAEoCFIKcmV0dXJuTnVsbA==');
@$core.Deprecated('Use g_GRecipeService_CountNullable_ReturnDescriptor instead')
const G_GRecipeService_CountNullable_Return$json = {
  '1': 'G_GRecipeService_CountNullable_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 1, '5': 5, '10': 'value'},
    {'1': 'value_has_value', '3': 2, '4': 1, '5': 8, '10': 'valueHasValue'},
  ],
};

/// Descriptor for `G_GRecipeService_CountNullable_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_CountNullable_ReturnDescriptor =
    $convert.base64Decode(
        'CiVHX0dSZWNpcGVTZXJ2aWNlX0NvdW50TnVsbGFibGVfUmV0dXJuEhQKBXZhbHVlGAEgASgFUgV2YWx1ZRImCg92YWx1ZV9oYXNfdmFsdWUYAiABKAhSDXZhbHVlSGFzVmFsdWU=');
@$core.Deprecated('Use g_GRecipeService_Reindex_ParametersDescriptor instead')
const G_GRecipeService_Reindex_Parameters$json = {
  '1': 'G_GRecipeService_Reindex_Parameters',
};

/// Descriptor for `G_GRecipeService_Reindex_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_Reindex_ParametersDescriptor =
    $convert
        .base64Decode('CiNHX0dSZWNpcGVTZXJ2aWNlX1JlaW5kZXhfUGFyYW1ldGVycw==');
@$core.Deprecated('Use g_GRecipeService_Reindex_ReturnDescriptor instead')
const G_GRecipeService_Reindex_Return$json = {
  '1': 'G_GRecipeService_Reindex_Return',
};

/// Descriptor for `G_GRecipeService_Reindex_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_Reindex_ReturnDescriptor =
    $convert.base64Decode('Ch9HX0dSZWNpcGVTZXJ2aWNlX1JlaW5kZXhfUmV0dXJu');
@$core.Deprecated(
    'Use g_GRecipeService_GetMainRecipeType_ParametersDescriptor instead')
const G_GRecipeService_GetMainRecipeType_Parameters$json = {
  '1': 'G_GRecipeService_GetMainRecipeType_Parameters',
};

/// Descriptor for `G_GRecipeService_GetMainRecipeType_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetMainRecipeType_ParametersDescriptor =
    $convert.base64Decode(
        'Ci1HX0dSZWNpcGVTZXJ2aWNlX0dldE1haW5SZWNpcGVUeXBlX1BhcmFtZXRlcnM=');
@$core.Deprecated(
    'Use g_GRecipeService_GetMainRecipeType_ReturnDescriptor instead')
const G_GRecipeService_GetMainRecipeType_Return$json = {
  '1': 'G_GRecipeService_GetMainRecipeType_Return',
  '2': [
    {
      '1': 'value',
      '3': 1,
      '4': 1,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'value'
    },
  ],
};

/// Descriptor for `G_GRecipeService_GetMainRecipeType_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetMainRecipeType_ReturnDescriptor = $convert.base64Decode(
        'CilHX0dSZWNpcGVTZXJ2aWNlX0dldE1haW5SZWNpcGVUeXBlX1JldHVybhIjCgV2YWx1ZRgBIAEoDjINLkdSZWNpcGVUeXBlc1IFdmFsdWU=');
@$core.Deprecated(
    'Use g_GRecipeService_GetMainRecipeTypeNullable_ParametersDescriptor instead')
const G_GRecipeService_GetMainRecipeTypeNullable_Parameters$json = {
  '1': 'G_GRecipeService_GetMainRecipeTypeNullable_Parameters',
};

/// Descriptor for `G_GRecipeService_GetMainRecipeTypeNullable_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetMainRecipeTypeNullable_ParametersDescriptor =
    $convert.base64Decode(
        'CjVHX0dSZWNpcGVTZXJ2aWNlX0dldE1haW5SZWNpcGVUeXBlTnVsbGFibGVfUGFyYW1ldGVycw==');
@$core.Deprecated(
    'Use g_GRecipeService_GetMainRecipeTypeNullable_ReturnDescriptor instead')
const G_GRecipeService_GetMainRecipeTypeNullable_Return$json = {
  '1': 'G_GRecipeService_GetMainRecipeTypeNullable_Return',
  '2': [
    {
      '1': 'value',
      '3': 1,
      '4': 1,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'value'
    },
    {'1': 'value_has_value', '3': 2, '4': 1, '5': 8, '10': 'valueHasValue'},
  ],
};

/// Descriptor for `G_GRecipeService_GetMainRecipeTypeNullable_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetMainRecipeTypeNullable_ReturnDescriptor =
    $convert.base64Decode(
        'CjFHX0dSZWNpcGVTZXJ2aWNlX0dldE1haW5SZWNpcGVUeXBlTnVsbGFibGVfUmV0dXJuEiMKBXZhbHVlGAEgASgOMg0uR1JlY2lwZVR5cGVzUgV2YWx1ZRImCg92YWx1ZV9oYXNfdmFsdWUYAiABKAhSDXZhbHVlSGFzVmFsdWU=');
@$core.Deprecated(
    'Use g_GRecipeService_GetRecipeTypeList_ParametersDescriptor instead')
const G_GRecipeService_GetRecipeTypeList_Parameters$json = {
  '1': 'G_GRecipeService_GetRecipeTypeList_Parameters',
};

/// Descriptor for `G_GRecipeService_GetRecipeTypeList_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetRecipeTypeList_ParametersDescriptor =
    $convert.base64Decode(
        'Ci1HX0dSZWNpcGVTZXJ2aWNlX0dldFJlY2lwZVR5cGVMaXN0X1BhcmFtZXRlcnM=');
@$core.Deprecated(
    'Use g_GRecipeService_GetRecipeTypeList_ReturnDescriptor instead')
const G_GRecipeService_GetRecipeTypeList_Return$json = {
  '1': 'G_GRecipeService_GetRecipeTypeList_Return',
  '2': [
    {
      '1': 'value',
      '3': 1,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'value'
    },
  ],
};

/// Descriptor for `G_GRecipeService_GetRecipeTypeList_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetRecipeTypeList_ReturnDescriptor = $convert.base64Decode(
        'CilHX0dSZWNpcGVTZXJ2aWNlX0dldFJlY2lwZVR5cGVMaXN0X1JldHVybhIjCgV2YWx1ZRgBIAMoDjINLkdSZWNpcGVUeXBlc1IFdmFsdWU=');
@$core.Deprecated(
    'Use g_GRecipeService_GetRecipeTypeListNullable_ParametersDescriptor instead')
const G_GRecipeService_GetRecipeTypeListNullable_Parameters$json = {
  '1': 'G_GRecipeService_GetRecipeTypeListNullable_Parameters',
};

/// Descriptor for `G_GRecipeService_GetRecipeTypeListNullable_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetRecipeTypeListNullable_ParametersDescriptor =
    $convert.base64Decode(
        'CjVHX0dSZWNpcGVTZXJ2aWNlX0dldFJlY2lwZVR5cGVMaXN0TnVsbGFibGVfUGFyYW1ldGVycw==');
@$core.Deprecated(
    'Use g_GRecipeService_GetRecipeTypeListNullable_ReturnDescriptor instead')
const G_GRecipeService_GetRecipeTypeListNullable_Return$json = {
  '1': 'G_GRecipeService_GetRecipeTypeListNullable_Return',
  '2': [
    {
      '1': 'value',
      '3': 1,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'value'
    },
    {'1': 'value_has_value', '3': 2, '4': 1, '5': 8, '10': 'valueHasValue'},
  ],
};

/// Descriptor for `G_GRecipeService_GetRecipeTypeListNullable_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetRecipeTypeListNullable_ReturnDescriptor =
    $convert.base64Decode(
        'CjFHX0dSZWNpcGVTZXJ2aWNlX0dldFJlY2lwZVR5cGVMaXN0TnVsbGFibGVfUmV0dXJuEiMKBXZhbHVlGAEgAygOMg0uR1JlY2lwZVR5cGVzUgV2YWx1ZRImCg92YWx1ZV9oYXNfdmFsdWUYAiABKAhSDXZhbHVlSGFzVmFsdWU=');
@$core.Deprecated(
    'Use g_GRecipeService_GetListOfInts_ParametersDescriptor instead')
const G_GRecipeService_GetListOfInts_Parameters$json = {
  '1': 'G_GRecipeService_GetListOfInts_Parameters',
};

/// Descriptor for `G_GRecipeService_GetListOfInts_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetListOfInts_ParametersDescriptor = $convert.base64Decode(
        'CilHX0dSZWNpcGVTZXJ2aWNlX0dldExpc3RPZkludHNfUGFyYW1ldGVycw==');
@$core.Deprecated('Use g_GRecipeService_GetListOfInts_ReturnDescriptor instead')
const G_GRecipeService_GetListOfInts_Return$json = {
  '1': 'G_GRecipeService_GetListOfInts_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 3, '5': 5, '10': 'value'},
  ],
};

/// Descriptor for `G_GRecipeService_GetListOfInts_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List g_GRecipeService_GetListOfInts_ReturnDescriptor =
    $convert.base64Decode(
        'CiVHX0dSZWNpcGVTZXJ2aWNlX0dldExpc3RPZkludHNfUmV0dXJuEhQKBXZhbHVlGAEgAygFUgV2YWx1ZQ==');
@$core.Deprecated(
    'Use g_GRecipeService_GetListOfIntsNullable_ParametersDescriptor instead')
const G_GRecipeService_GetListOfIntsNullable_Parameters$json = {
  '1': 'G_GRecipeService_GetListOfIntsNullable_Parameters',
};

/// Descriptor for `G_GRecipeService_GetListOfIntsNullable_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetListOfIntsNullable_ParametersDescriptor =
    $convert.base64Decode(
        'CjFHX0dSZWNpcGVTZXJ2aWNlX0dldExpc3RPZkludHNOdWxsYWJsZV9QYXJhbWV0ZXJz');
@$core.Deprecated(
    'Use g_GRecipeService_GetListOfIntsNullable_ReturnDescriptor instead')
const G_GRecipeService_GetListOfIntsNullable_Return$json = {
  '1': 'G_GRecipeService_GetListOfIntsNullable_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 3, '5': 5, '10': 'value'},
    {'1': 'value_has_value', '3': 2, '4': 1, '5': 8, '10': 'valueHasValue'},
  ],
};

/// Descriptor for `G_GRecipeService_GetListOfIntsNullable_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_GetListOfIntsNullable_ReturnDescriptor =
    $convert.base64Decode(
        'Ci1HX0dSZWNpcGVTZXJ2aWNlX0dldExpc3RPZkludHNOdWxsYWJsZV9SZXR1cm4SFAoFdmFsdWUYASADKAVSBXZhbHVlEiYKD3ZhbHVlX2hhc192YWx1ZRgCIAEoCFINdmFsdWVIYXNWYWx1ZQ==');
@$core.Deprecated(
    'Use g_GRecipeService_ReceiveLotsOfArgs_ParametersDescriptor instead')
const G_GRecipeService_ReceiveLotsOfArgs_Parameters$json = {
  '1': 'G_GRecipeService_ReceiveLotsOfArgs_Parameters',
  '2': [
    {'1': 'p_string', '3': 1, '4': 1, '5': 9, '10': 'pString'},
    {'1': 'p_int', '3': 2, '4': 1, '5': 5, '10': 'pInt'},
    {
      '1': 'p_recipe_types',
      '3': 3,
      '4': 1,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pRecipeTypes'
    },
    {
      '1': 'p_recipe',
      '3': 4,
      '4': 1,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pRecipe'
    },
    {'1': 'p_list_strings', '3': 5, '4': 3, '5': 9, '10': 'pListStrings'},
    {'1': 'p_list_ints', '3': 6, '4': 3, '5': 5, '10': 'pListInts'},
    {
      '1': 'p_list_recipe_types',
      '3': 7,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pListRecipeTypes'
    },
    {
      '1': 'p_list_recipes',
      '3': 8,
      '4': 3,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pListRecipes'
    },
    {'1': 'p_set_string', '3': 9, '4': 3, '5': 9, '10': 'pSetString'},
    {'1': 'p_set_int', '3': 10, '4': 3, '5': 5, '10': 'pSetInt'},
    {
      '1': 'p_set_recipe_types',
      '3': 11,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pSetRecipeTypes'
    },
    {
      '1': 'p_set_recipe',
      '3': 12,
      '4': 3,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pSetRecipe'
    },
    {
      '1': 'p_iterable_string',
      '3': 13,
      '4': 3,
      '5': 9,
      '10': 'pIterableString'
    },
    {'1': 'p_iterable_int', '3': 14, '4': 3, '5': 5, '10': 'pIterableInt'},
    {
      '1': 'p_iterable_recipe_types',
      '3': 15,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pIterableRecipeTypes'
    },
    {
      '1': 'p_iterable_recipe',
      '3': 16,
      '4': 3,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pIterableRecipe'
    },
  ],
};

/// Descriptor for `G_GRecipeService_ReceiveLotsOfArgs_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_ReceiveLotsOfArgs_ParametersDescriptor =
    $convert.base64Decode(
        'Ci1HX0dSZWNpcGVTZXJ2aWNlX1JlY2VpdmVMb3RzT2ZBcmdzX1BhcmFtZXRlcnMSGQoIcF9zdHJpbmcYASABKAlSB3BTdHJpbmcSEwoFcF9pbnQYAiABKAVSBHBJbnQSMwoOcF9yZWNpcGVfdHlwZXMYAyABKA4yDS5HUmVjaXBlVHlwZXNSDHBSZWNpcGVUeXBlcxIjCghwX3JlY2lwZRgEIAEoCzIILkdSZWNpcGVSB3BSZWNpcGUSJAoOcF9saXN0X3N0cmluZ3MYBSADKAlSDHBMaXN0U3RyaW5ncxIeCgtwX2xpc3RfaW50cxgGIAMoBVIJcExpc3RJbnRzEjwKE3BfbGlzdF9yZWNpcGVfdHlwZXMYByADKA4yDS5HUmVjaXBlVHlwZXNSEHBMaXN0UmVjaXBlVHlwZXMSLgoOcF9saXN0X3JlY2lwZXMYCCADKAsyCC5HUmVjaXBlUgxwTGlzdFJlY2lwZXMSIAoMcF9zZXRfc3RyaW5nGAkgAygJUgpwU2V0U3RyaW5nEhoKCXBfc2V0X2ludBgKIAMoBVIHcFNldEludBI6ChJwX3NldF9yZWNpcGVfdHlwZXMYCyADKA4yDS5HUmVjaXBlVHlwZXNSD3BTZXRSZWNpcGVUeXBlcxIqCgxwX3NldF9yZWNpcGUYDCADKAsyCC5HUmVjaXBlUgpwU2V0UmVjaXBlEioKEXBfaXRlcmFibGVfc3RyaW5nGA0gAygJUg9wSXRlcmFibGVTdHJpbmcSJAoOcF9pdGVyYWJsZV9pbnQYDiADKAVSDHBJdGVyYWJsZUludBJEChdwX2l0ZXJhYmxlX3JlY2lwZV90eXBlcxgPIAMoDjINLkdSZWNpcGVUeXBlc1IUcEl0ZXJhYmxlUmVjaXBlVHlwZXMSNAoRcF9pdGVyYWJsZV9yZWNpcGUYECADKAsyCC5HUmVjaXBlUg9wSXRlcmFibGVSZWNpcGU=');
@$core.Deprecated(
    'Use g_GRecipeService_ReceiveLotsOfArgs_ReturnDescriptor instead')
const G_GRecipeService_ReceiveLotsOfArgs_Return$json = {
  '1': 'G_GRecipeService_ReceiveLotsOfArgs_Return',
};

/// Descriptor for `G_GRecipeService_ReceiveLotsOfArgs_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_ReceiveLotsOfArgs_ReturnDescriptor = $convert.base64Decode(
        'CilHX0dSZWNpcGVTZXJ2aWNlX1JlY2VpdmVMb3RzT2ZBcmdzX1JldHVybg==');
@$core.Deprecated(
    'Use g_GRecipeService_ReceiveLotsOfNullableArgs_ParametersDescriptor instead')
const G_GRecipeService_ReceiveLotsOfNullableArgs_Parameters$json = {
  '1': 'G_GRecipeService_ReceiveLotsOfNullableArgs_Parameters',
  '2': [
    {'1': 'p_string', '3': 1, '4': 1, '5': 9, '10': 'pString'},
    {
      '1': 'p_string_has_value',
      '3': 2,
      '4': 1,
      '5': 8,
      '10': 'pStringHasValue'
    },
    {'1': 'p_int', '3': 3, '4': 1, '5': 5, '10': 'pInt'},
    {'1': 'p_int_has_value', '3': 4, '4': 1, '5': 8, '10': 'pIntHasValue'},
    {
      '1': 'p_recipe_types',
      '3': 5,
      '4': 1,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pRecipeTypes'
    },
    {
      '1': 'p_recipe_types_has_value',
      '3': 6,
      '4': 1,
      '5': 8,
      '10': 'pRecipeTypesHasValue'
    },
    {
      '1': 'p_recipe',
      '3': 7,
      '4': 1,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pRecipe'
    },
    {
      '1': 'p_recipe_has_value',
      '3': 8,
      '4': 1,
      '5': 8,
      '10': 'pRecipeHasValue'
    },
    {'1': 'p_list_strings', '3': 9, '4': 3, '5': 9, '10': 'pListStrings'},
    {
      '1': 'p_list_strings_has_value',
      '3': 10,
      '4': 1,
      '5': 8,
      '10': 'pListStringsHasValue'
    },
    {'1': 'p_list_ints', '3': 11, '4': 3, '5': 5, '10': 'pListInts'},
    {
      '1': 'p_list_ints_has_value',
      '3': 12,
      '4': 1,
      '5': 8,
      '10': 'pListIntsHasValue'
    },
    {
      '1': 'p_list_recipe_types',
      '3': 13,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pListRecipeTypes'
    },
    {
      '1': 'p_list_recipe_types_has_value',
      '3': 14,
      '4': 1,
      '5': 8,
      '10': 'pListRecipeTypesHasValue'
    },
    {
      '1': 'p_list_recipes',
      '3': 15,
      '4': 3,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pListRecipes'
    },
    {
      '1': 'p_list_recipes_has_value',
      '3': 16,
      '4': 1,
      '5': 8,
      '10': 'pListRecipesHasValue'
    },
    {'1': 'p_set_string', '3': 17, '4': 3, '5': 9, '10': 'pSetString'},
    {
      '1': 'p_set_string_has_value',
      '3': 18,
      '4': 1,
      '5': 8,
      '10': 'pSetStringHasValue'
    },
    {'1': 'p_set_int', '3': 19, '4': 3, '5': 5, '10': 'pSetInt'},
    {
      '1': 'p_set_int_has_value',
      '3': 20,
      '4': 1,
      '5': 8,
      '10': 'pSetIntHasValue'
    },
    {
      '1': 'p_set_recipe_types',
      '3': 21,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pSetRecipeTypes'
    },
    {
      '1': 'p_set_recipe_types_has_value',
      '3': 22,
      '4': 1,
      '5': 8,
      '10': 'pSetRecipeTypesHasValue'
    },
    {
      '1': 'p_set_recipe',
      '3': 23,
      '4': 3,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pSetRecipe'
    },
    {
      '1': 'p_set_recipe_has_value',
      '3': 24,
      '4': 1,
      '5': 8,
      '10': 'pSetRecipeHasValue'
    },
    {
      '1': 'p_iterable_string',
      '3': 25,
      '4': 3,
      '5': 9,
      '10': 'pIterableString'
    },
    {
      '1': 'p_iterable_string_has_value',
      '3': 26,
      '4': 1,
      '5': 8,
      '10': 'pIterableStringHasValue'
    },
    {'1': 'p_iterable_int', '3': 27, '4': 3, '5': 5, '10': 'pIterableInt'},
    {
      '1': 'p_iterable_int_has_value',
      '3': 28,
      '4': 1,
      '5': 8,
      '10': 'pIterableIntHasValue'
    },
    {
      '1': 'p_iterable_recipe_types',
      '3': 29,
      '4': 3,
      '5': 14,
      '6': '.GRecipeTypes',
      '10': 'pIterableRecipeTypes'
    },
    {
      '1': 'p_iterable_recipe_types_has_value',
      '3': 30,
      '4': 1,
      '5': 8,
      '10': 'pIterableRecipeTypesHasValue'
    },
    {
      '1': 'p_iterable_recipe',
      '3': 31,
      '4': 3,
      '5': 11,
      '6': '.GRecipe',
      '10': 'pIterableRecipe'
    },
    {
      '1': 'p_iterable_recipe_has_value',
      '3': 32,
      '4': 1,
      '5': 8,
      '10': 'pIterableRecipeHasValue'
    },
  ],
};

/// Descriptor for `G_GRecipeService_ReceiveLotsOfNullableArgs_Parameters`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_ReceiveLotsOfNullableArgs_ParametersDescriptor =
    $convert.base64Decode(
        'CjVHX0dSZWNpcGVTZXJ2aWNlX1JlY2VpdmVMb3RzT2ZOdWxsYWJsZUFyZ3NfUGFyYW1ldGVycxIZCghwX3N0cmluZxgBIAEoCVIHcFN0cmluZxIrChJwX3N0cmluZ19oYXNfdmFsdWUYAiABKAhSD3BTdHJpbmdIYXNWYWx1ZRITCgVwX2ludBgDIAEoBVIEcEludBIlCg9wX2ludF9oYXNfdmFsdWUYBCABKAhSDHBJbnRIYXNWYWx1ZRIzCg5wX3JlY2lwZV90eXBlcxgFIAEoDjINLkdSZWNpcGVUeXBlc1IMcFJlY2lwZVR5cGVzEjYKGHBfcmVjaXBlX3R5cGVzX2hhc192YWx1ZRgGIAEoCFIUcFJlY2lwZVR5cGVzSGFzVmFsdWUSIwoIcF9yZWNpcGUYByABKAsyCC5HUmVjaXBlUgdwUmVjaXBlEisKEnBfcmVjaXBlX2hhc192YWx1ZRgIIAEoCFIPcFJlY2lwZUhhc1ZhbHVlEiQKDnBfbGlzdF9zdHJpbmdzGAkgAygJUgxwTGlzdFN0cmluZ3MSNgoYcF9saXN0X3N0cmluZ3NfaGFzX3ZhbHVlGAogASgIUhRwTGlzdFN0cmluZ3NIYXNWYWx1ZRIeCgtwX2xpc3RfaW50cxgLIAMoBVIJcExpc3RJbnRzEjAKFXBfbGlzdF9pbnRzX2hhc192YWx1ZRgMIAEoCFIRcExpc3RJbnRzSGFzVmFsdWUSPAoTcF9saXN0X3JlY2lwZV90eXBlcxgNIAMoDjINLkdSZWNpcGVUeXBlc1IQcExpc3RSZWNpcGVUeXBlcxI/Ch1wX2xpc3RfcmVjaXBlX3R5cGVzX2hhc192YWx1ZRgOIAEoCFIYcExpc3RSZWNpcGVUeXBlc0hhc1ZhbHVlEi4KDnBfbGlzdF9yZWNpcGVzGA8gAygLMgguR1JlY2lwZVIMcExpc3RSZWNpcGVzEjYKGHBfbGlzdF9yZWNpcGVzX2hhc192YWx1ZRgQIAEoCFIUcExpc3RSZWNpcGVzSGFzVmFsdWUSIAoMcF9zZXRfc3RyaW5nGBEgAygJUgpwU2V0U3RyaW5nEjIKFnBfc2V0X3N0cmluZ19oYXNfdmFsdWUYEiABKAhSEnBTZXRTdHJpbmdIYXNWYWx1ZRIaCglwX3NldF9pbnQYEyADKAVSB3BTZXRJbnQSLAoTcF9zZXRfaW50X2hhc192YWx1ZRgUIAEoCFIPcFNldEludEhhc1ZhbHVlEjoKEnBfc2V0X3JlY2lwZV90eXBlcxgVIAMoDjINLkdSZWNpcGVUeXBlc1IPcFNldFJlY2lwZVR5cGVzEj0KHHBfc2V0X3JlY2lwZV90eXBlc19oYXNfdmFsdWUYFiABKAhSF3BTZXRSZWNpcGVUeXBlc0hhc1ZhbHVlEioKDHBfc2V0X3JlY2lwZRgXIAMoCzIILkdSZWNpcGVSCnBTZXRSZWNpcGUSMgoWcF9zZXRfcmVjaXBlX2hhc192YWx1ZRgYIAEoCFIScFNldFJlY2lwZUhhc1ZhbHVlEioKEXBfaXRlcmFibGVfc3RyaW5nGBkgAygJUg9wSXRlcmFibGVTdHJpbmcSPAobcF9pdGVyYWJsZV9zdHJpbmdfaGFzX3ZhbHVlGBogASgIUhdwSXRlcmFibGVTdHJpbmdIYXNWYWx1ZRIkCg5wX2l0ZXJhYmxlX2ludBgbIAMoBVIMcEl0ZXJhYmxlSW50EjYKGHBfaXRlcmFibGVfaW50X2hhc192YWx1ZRgcIAEoCFIUcEl0ZXJhYmxlSW50SGFzVmFsdWUSRAoXcF9pdGVyYWJsZV9yZWNpcGVfdHlwZXMYHSADKA4yDS5HUmVjaXBlVHlwZXNSFHBJdGVyYWJsZVJlY2lwZVR5cGVzEkcKIXBfaXRlcmFibGVfcmVjaXBlX3R5cGVzX2hhc192YWx1ZRgeIAEoCFIccEl0ZXJhYmxlUmVjaXBlVHlwZXNIYXNWYWx1ZRI0ChFwX2l0ZXJhYmxlX3JlY2lwZRgfIAMoCzIILkdSZWNpcGVSD3BJdGVyYWJsZVJlY2lwZRI8ChtwX2l0ZXJhYmxlX3JlY2lwZV9oYXNfdmFsdWUYICABKAhSF3BJdGVyYWJsZVJlY2lwZUhhc1ZhbHVl');
@$core.Deprecated(
    'Use g_GRecipeService_ReceiveLotsOfNullableArgs_ReturnDescriptor instead')
const G_GRecipeService_ReceiveLotsOfNullableArgs_Return$json = {
  '1': 'G_GRecipeService_ReceiveLotsOfNullableArgs_Return',
  '2': [
    {'1': 'value', '3': 1, '4': 3, '5': 5, '10': 'value'},
  ],
};

/// Descriptor for `G_GRecipeService_ReceiveLotsOfNullableArgs_Return`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List
    g_GRecipeService_ReceiveLotsOfNullableArgs_ReturnDescriptor =
    $convert.base64Decode(
        'CjFHX0dSZWNpcGVTZXJ2aWNlX1JlY2VpdmVMb3RzT2ZOdWxsYWJsZUFyZ3NfUmV0dXJuEhQKBXZhbHVlGAEgAygFUgV2YWx1ZQ==');
