///
//  Generated code. Do not modify.
//  source: constructors_host.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;

@$core.Deprecated('Use gConstructObject1Descriptor instead')
const GConstructObject1$json = {
  '1': 'GConstructObject1',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'number', '3': 2, '4': 1, '5': 5, '10': 'number'},
  ],
};

/// Descriptor for `GConstructObject1`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject1Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0MRISCgRuYW1lGAEgASgJUgRuYW1lEhYKBm51bWJlchgCIAEoBVIGbnVtYmVy');
@$core.Deprecated('Use gListOfConstructObject1Descriptor instead')
const GListOfConstructObject1$json = {
  '1': 'GListOfConstructObject1',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject1',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject1`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject1Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0MRIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3QxUgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject2Descriptor instead')
const GConstructObject2$json = {
  '1': 'GConstructObject2',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'number', '3': 2, '4': 1, '5': 5, '10': 'number'},
  ],
};

/// Descriptor for `GConstructObject2`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject2Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0MhISCgRuYW1lGAEgASgJUgRuYW1lEhYKBm51bWJlchgCIAEoBVIGbnVtYmVy');
@$core.Deprecated('Use gListOfConstructObject2Descriptor instead')
const GListOfConstructObject2$json = {
  '1': 'GListOfConstructObject2',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject2',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject2`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject2Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0MhIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3QyUgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject3Descriptor instead')
const GConstructObject3$json = {
  '1': 'GConstructObject3',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'number', '3': 2, '4': 1, '5': 5, '10': 'number'},
  ],
};

/// Descriptor for `GConstructObject3`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject3Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0MxISCgRuYW1lGAEgASgJUgRuYW1lEhYKBm51bWJlchgCIAEoBVIGbnVtYmVy');
@$core.Deprecated('Use gListOfConstructObject3Descriptor instead')
const GListOfConstructObject3$json = {
  '1': 'GListOfConstructObject3',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject3',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject3`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject3Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0MxIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3QzUgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject4Descriptor instead')
const GConstructObject4$json = {
  '1': 'GConstructObject4',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'number', '3': 2, '4': 1, '5': 5, '10': 'number'},
  ],
};

/// Descriptor for `GConstructObject4`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject4Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0NBISCgRuYW1lGAEgASgJUgRuYW1lEhYKBm51bWJlchgCIAEoBVIGbnVtYmVy');
@$core.Deprecated('Use gListOfConstructObject4Descriptor instead')
const GListOfConstructObject4$json = {
  '1': 'GListOfConstructObject4',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject4',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject4`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject4Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0NBIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3Q0UgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject5Descriptor instead')
const GConstructObject5$json = {
  '1': 'GConstructObject5',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'number', '3': 2, '4': 1, '5': 5, '10': 'number'},
  ],
};

/// Descriptor for `GConstructObject5`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject5Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0NRISCgRuYW1lGAEgASgJUgRuYW1lEhYKBm51bWJlchgCIAEoBVIGbnVtYmVy');
@$core.Deprecated('Use gListOfConstructObject5Descriptor instead')
const GListOfConstructObject5$json = {
  '1': 'GListOfConstructObject5',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject5',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject5`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject5Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0NRIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3Q1UgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject6Descriptor instead')
const GConstructObject6$json = {
  '1': 'GConstructObject6',
  '2': [
    {'1': 'name', '3': 1, '4': 1, '5': 9, '10': 'name'},
    {'1': 'name_has_value', '3': 2, '4': 1, '5': 8, '10': 'nameHasValue'},
    {'1': 'number', '3': 3, '4': 1, '5': 5, '10': 'number'},
    {'1': 'number_has_value', '3': 4, '4': 1, '5': 8, '10': 'numberHasValue'},
  ],
};

/// Descriptor for `GConstructObject6`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject6Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0NhISCgRuYW1lGAEgASgJUgRuYW1lEiQKDm5hbWVfaGFzX3ZhbHVlGAIgASgIUgxuYW1lSGFzVmFsdWUSFgoGbnVtYmVyGAMgASgFUgZudW1iZXISKAoQbnVtYmVyX2hhc192YWx1ZRgEIAEoCFIObnVtYmVySGFzVmFsdWU=');
@$core.Deprecated('Use gListOfConstructObject6Descriptor instead')
const GListOfConstructObject6$json = {
  '1': 'GListOfConstructObject6',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject6',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject6`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject6Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0NhIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3Q2UgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject7Descriptor instead')
const GConstructObject7$json = {
  '1': 'GConstructObject7',
  '2': [
    {'1': 'number', '3': 1, '4': 1, '5': 5, '10': 'number'},
    {'1': 'number_has_value', '3': 2, '4': 1, '5': 8, '10': 'numberHasValue'},
    {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `GConstructObject7`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject7Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0NxIWCgZudW1iZXIYASABKAVSBm51bWJlchIoChBudW1iZXJfaGFzX3ZhbHVlGAIgASgIUg5udW1iZXJIYXNWYWx1ZRISCgRuYW1lGAMgASgJUgRuYW1l');
@$core.Deprecated('Use gListOfConstructObject7Descriptor instead')
const GListOfConstructObject7$json = {
  '1': 'GListOfConstructObject7',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject7',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject7`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject7Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0NxIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3Q3UgVpdGVtcw==');
@$core.Deprecated('Use gConstructObject8Descriptor instead')
const GConstructObject8$json = {
  '1': 'GConstructObject8',
  '2': [
    {'1': 'number', '3': 1, '4': 1, '5': 5, '10': 'number'},
    {'1': 'number_has_value', '3': 2, '4': 1, '5': 8, '10': 'numberHasValue'},
    {'1': 'name', '3': 3, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `GConstructObject8`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gConstructObject8Descriptor = $convert.base64Decode(
    'ChFHQ29uc3RydWN0T2JqZWN0OBIWCgZudW1iZXIYASABKAVSBm51bWJlchIoChBudW1iZXJfaGFzX3ZhbHVlGAIgASgIUg5udW1iZXJIYXNWYWx1ZRISCgRuYW1lGAMgASgJUgRuYW1l');
@$core.Deprecated('Use gListOfConstructObject8Descriptor instead')
const GListOfConstructObject8$json = {
  '1': 'GListOfConstructObject8',
  '2': [
    {
      '1': 'items',
      '3': 1,
      '4': 3,
      '5': 11,
      '6': '.GConstructObject8',
      '10': 'items'
    },
  ],
};

/// Descriptor for `GListOfConstructObject8`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gListOfConstructObject8Descriptor =
    $convert.base64Decode(
        'ChdHTGlzdE9mQ29uc3RydWN0T2JqZWN0OBIoCgVpdGVtcxgBIAMoCzISLkdDb25zdHJ1Y3RPYmplY3Q4UgVpdGVtcw==');
