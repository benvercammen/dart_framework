## 2.2.1

 - Make FieldDescriptor.isKey only return true for numeric or string values, not for objects
 - Improved constructor support in MapMapGenerator

## 2.2.0

### Changes
- Moved source code repository from GitHub to GitLab
- Adopted Melos and GitLab as CI solution
- Changed license to MPL

### Rationale for source code hosting change

The change from source code repository is in protest by this project's initial and main author with what he views as [GitHub's extremely week response](https://github.blog/2022-03-02-our-response-to-the-war-in-ukraine/) to the carnage going on in Ukraine by Russia. He would have expected at a minimum for any new business in Russia and Belarus to be suspended, which was incidentally [GitLab's course of action](https://about.gitlab.com/blog/2022/03/11/gitlab-actions-to-date-regarding-russian-invasion-of-ukraine/#suspending-new-business-in-russia-and-belarus).


## 2.1.3

- Updated dependencies

## 2.1.2

- Fixed ConstantReader extensions

## 2.1.1

- Added support for DateTimeRepresentation

## 2.1.0

- Moved TimePrecision from this package to a dependency

## 2.0.3

- Added extension to read TimePrecision from ConstantReader

## 2.0.2

- Added TimePrecision enum

## 2.0.1

- Upgraded dependencies

## 2.0.0

- Added support for iterables and sets

## 1.0.0

- Initial published version

## 1.0.1

- Updated dependencies

## 1.0.2-dev

- Updated dependencies


