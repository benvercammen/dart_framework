import 'package:map_mapper_annotations/map_mapper_annotations.dart';

part 'events.g.dart';

@mapMapped
class AggregateId {

  final String id;
  final String type;

  AggregateId.of(this.id, this.type);
}

@MapMapped(knownSubClasses: [Command, Event])
abstract class Message {

  final String messageId;
  final AggregateId? aggregateId;
  final DateTime timestamp;
  final String payloadType;
  final dynamic payload;

  const Message({
    required this.messageId,
    required this.aggregateId,
    required this.timestamp,
    required this.payloadType,
    required this.payload,
  });
}

@mapMapped
class Command extends Message {

  final String? userId;

  Command({
    required String messageId,
    AggregateId? aggregateId,
    required DateTime timestamp,
    required String payloadType,
    required dynamic payload,
    this.userId
  }) : super(
      messageId: messageId,
      aggregateId: aggregateId,
      timestamp: timestamp,
      payloadType: payloadType,
      payload: payload);

  String get commandId => messageId;
}

@MapMapped(knownSubClasses: [IntegrationEvent])
class Event extends Message {

  final String? userId;

  const Event({
    required String messageId,
    required DateTime timestamp,
    this.userId,
    required AggregateId? aggregateId,
    required String payloadType,
    required dynamic payload,
  })  : super(
          messageId: messageId,
          aggregateId: aggregateId,
          timestamp: timestamp,
          payloadType: payloadType,
          payload: payload);

  String get eventId => messageId;
}


/// Don't include "aggregateId" or "userId", it will always be null...
@mapMapped
class IntegrationEvent extends Event {

  const IntegrationEvent({
    required String messageId,
    required DateTime timestamp,
    String? processId,
    required String payloadType,
    required dynamic payload,
  })
      :
        super(
          messageId: messageId,
          timestamp: timestamp,
          aggregateId: null,
          payloadType: payloadType,
          payload: payload);
}